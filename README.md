# Basic Web App with form authenticator symfony 5

## Create database

```php bin/console doctrine:database:create```

```php bin/console doctrine:migrations:migrate```

## Hydrate database

Manualy add a user- table "user"

ex : email - 'test@test.com' <br/>
     role - '["ROLE_USER"]' <br/>
     password - '$2y$13$oX68ky4bN3XaWfB9X6H2juwFrKmnShD49r59BKoncyJkcQTD5R42.'  #test <br/>


## Generate Password 

```php bin/console security:encode-password```